using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.WebApi.Configuration
{
    public static class RepositoryManagerConfiguration
    {
        public static void AddRepositoryManager(this IServiceCollection services)
        {
            services.AddScoped<ICustomerRepository, CustomerRepository>();
        }
    }
}