using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    [ApiController]
    [Route("users")]
    public class UserController : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;

        public UserController(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Возвращает пользователя по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор пользователя</param>
        /// <returns>Пользователь</returns>
        [HttpGet("{id:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Customer> GetUserById(int id)
        {
            var user = _customerRepository.GetCustomer(id);
            return user == null ? NotFound("User not found.") : Ok(user);
        }

        /// <summary>
        /// Добавляет пользователя в БД
        /// </summary>
        /// <param name="customer">Пользователь</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult AddUser([FromBody] Customer customer)
        {
            if (_customerRepository.GetCustomer(customer.Id) != null)
            {
                return Conflict($"User with id {customer.Id} already exist.");
            }
            
            _customerRepository.AddCustomer(customer);
            _customerRepository.SaveChanges();
            return Ok($"User with id {customer.Id} was added.");
        }
    }
}