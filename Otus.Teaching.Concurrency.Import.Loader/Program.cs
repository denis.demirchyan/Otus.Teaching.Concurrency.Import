﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static readonly string DataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers");
        private const int DataCount = 10;
        private const string GeneratorFilePath = "/home/poehavshy/RiderProjects/Otus.Teaching.Concurrency.Import/Otus.Teaching.Concurrency.Import.DataGenerator.App/bin/Release/net5.0/Otus.Teaching.Concurrency.Import.DataGenerator.App";
        private const int ThreadCount = 4;
        
        static void Main(string[] args)
        {
            Console.WriteLine($"Loader PID {Process.GetCurrentProcess().Id}...");

            var dataFilePath = GetDataFilePath();
            var dataFileExtension = GetFileExtension();
            var dataCount = GetDataCount();
            
            if (IsGenerateStartMethod()) GenerateCustomersDataFile(dataFilePath, dataFileExtension, dataCount);
            else GenerateCustomersDataFileProcess(dataFilePath, dataFileExtension, dataCount);

            var customers = ParseCustomersDataFile(dataFilePath, dataFileExtension);

            Console.WriteLine($"Получено {customers.Count} записей.");

            var threadCount = GetThreadCount();

            if (IsThreadPoolLoader()) new ThreadPoolDataLoader(customers, threadCount).LoadData();
            else new ThreadDataLoader(customers, threadCount).LoadData();
            
            Console.WriteLine("Записи добавлены в бд.");
        }

        static string GetFileExtension()
        {
            Console.WriteLine("Выберите тип файла. Xml/Csv (default Csv):");
            var userChoice = Console.ReadLine();

            if (string.IsNullOrEmpty(userChoice)) return "csv";

            var userChoiceLower = userChoice.ToLower();

            if (userChoiceLower is "xml" or "csv") return userChoiceLower;

            Console.WriteLine("Некорректный выбор.");
            return GetFileExtension();

        }

        static bool IsThreadPoolLoader()
        {
            Console.WriteLine("Запустить загрузку через ThreadPool? Yes/No (default Yes):");
            var userChoice = Console.ReadLine();

            if (string.IsNullOrEmpty(userChoice)) return true;

            var userChoiceLower = userChoice.ToLower();

            if (userChoiceLower.StartsWith('n')) return false;
            if (userChoiceLower.StartsWith('y')) return true;

            Console.WriteLine("Некорректный выбор.");
            return IsThreadPoolLoader();
        }

        static int GetThreadCount()
        {
            Console.WriteLine($"Введите количество нужных потоков (default {ThreadCount}):");
            var userInput = Console.ReadLine();
            return string.IsNullOrEmpty(userInput) ? ThreadCount : int.Parse(userInput);
        }
        
        static string GetDataFilePath()
        {
            Console.WriteLine($"Введите путь к генерируемому файлу без расширения (default {DataFilePath}):");
            var userInput = Console.ReadLine();
            return string.IsNullOrEmpty(userInput) ? DataFilePath : userInput;
        }
        
        static int GetDataCount()
        {
            Console.WriteLine($"Введите количество генерируемых данных (default {DataCount}):");
            var userInput = Console.ReadLine();
            return string.IsNullOrEmpty(userInput) ? DataCount : int.Parse(userInput);
        }
        
        static string GetGenerateSourceFilePath()
        {
            Console.WriteLine($"Введите полный путь к исполняемому файлу генератора (default {GeneratorFilePath}):");
            var userInput = Console.ReadLine();
            return string.IsNullOrEmpty(userInput) ? GeneratorFilePath : userInput;
        }

        static bool IsGenerateStartMethod()
        {
            Console.WriteLine("Запустить генератор отделным процессом? Yes/No (default No):");
            var userChoice = Console.ReadLine();

            if (string.IsNullOrEmpty(userChoice)) return true;

            var userChoiceLower = userChoice.ToLower();

            if (userChoiceLower.StartsWith('n')) return true;
            if (userChoiceLower.StartsWith('y')) return false;

            Console.WriteLine("Некорректный выбор.");
            return IsGenerateStartMethod();
        }

        static void GenerateCustomersDataFileProcess(string dataFilePath, string dataFileExtension ,int dataCount)
        {
            ProcessStartInfo processInfo = new()
            {
                FileName = GetGenerateSourceFilePath(),
                Arguments = $"{dataFilePath} {dataFileExtension} {dataCount}"
            };

            var process = Process.Start(processInfo);
            if (process != null)
            {
                Console.WriteLine($"Generator PID {process.Id}");
                process.WaitForExit();
            }
            else
            {
                Console.WriteLine("Процесс не создан.");
            }
        }
        
        static void GenerateCustomersDataFile(string dataFilePath, string dataFileExtension, int dataCount)
        {
            var generator = GeneratorFactory.GetGenerator(dataFilePath, dataFileExtension, dataCount);
            generator.Generate();
            
            Console.WriteLine($"Данные созданы в файле {dataFilePath}.{dataFileExtension}.");
        }

        static List<Customer> ParseCustomersDataFile(string dataFilePath, string dataFileExtension)
        {
            var parser = ParserFactory.GetParser(dataFilePath, dataFileExtension);
            return parser.Parse();
        }
    }
}