using System.Collections.Generic;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader.Dto
{
    public class ThreadPoolDataLoaderItem
    {
        public readonly List<Customer> Customers;
        public readonly WaitHandle WaitHandle;

        public ThreadPoolDataLoaderItem(List<Customer> customers, WaitHandle waitHandle)
        {
            Customers = customers;
            WaitHandle = waitHandle;
        }
    }
}