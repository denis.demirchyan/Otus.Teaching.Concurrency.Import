using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class ThreadDataLoader : IDataLoader
    {
        private readonly int _threadCount;
        private readonly List<Customer> _customers;
        private readonly List<Thread> _threads = new();
        public ThreadDataLoader(List<Customer> customers, int threadCount)
        {
            _customers = customers;
            _threadCount = threadCount;
        }

        public void LoadData()
        {
            var stopWatch = new Stopwatch();
            var customersChunks = CustomersSlice();
            var waitHandles = new WaitHandle[customersChunks.Count];

            var index = 0;
            stopWatch.Start();
            foreach (var customersChunk in customersChunks)
            {
                waitHandles[index] = new AutoResetEvent(false);

                var innerIndex = index;
                var thread = new Thread(() =>
                {
                    using var repository = new CustomerRepository();
                    foreach (var customer in customersChunk)
                    {
                        repository.AddCustomer(customer);
                    }

                    repository.SaveChanges();
                    var autoResetEvent = (AutoResetEvent) waitHandles[innerIndex];
                    autoResetEvent.Set();
                });
                thread.Start();
                _threads.Add(thread);
                ++index;
            }
            WaitHandle.WaitAll(waitHandles);
            stopWatch.Stop();
            
            Console.WriteLine($"Thread - {stopWatch.Elapsed}");
        }

        private List<List<Customer>> CustomersSlice()
        {
            var chunkSize = _customers.Count / _threadCount;
            if (_customers.Count % _threadCount != 0)
            {
                ++chunkSize;
            }

            var customersChunks = new List<List<Customer>>();

            for (var i = 0; i < _customers.Count; i += chunkSize)
            {
                customersChunks.Add(GetCustomersChunk(_customers, i, chunkSize));
            }
            
            return customersChunks;
        }

        private static List<Customer> GetCustomersChunk(List<Customer> source, int start, int count)
        {
            if (start + count > source.Count)
            {
                count = source.Count - start;
            }
            var chunk = new Customer[count];
            source.CopyTo(start, chunk, 0, count);
            return chunk.ToList();
        }
    }
}