#nullable enable
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader.Dto;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class ThreadPoolDataLoader : IDataLoader
    {
        private readonly int _threadCount;
        private readonly List<Customer> _customers;

        public ThreadPoolDataLoader(List<Customer> customers, int threadCount)
        {
            _customers = customers;
            _threadCount = threadCount;
        }

        public void LoadData()
        {
            var stopWatch = new Stopwatch();
            var customersChunks = CustomersSlice();

            var waitHandles = new WaitHandle[customersChunks.Count];

            var index = 0;
            stopWatch.Start();
            foreach (var customers in customersChunks)
            {
                waitHandles[index] = new AutoResetEvent(false);

                var item = new ThreadPoolDataLoaderItem(customers, waitHandles[index]);

                ThreadPool.QueueUserWorkItem(LoadDataWithThreadPool, item);
                    
                ++index;
            }
            WaitHandle.WaitAll(waitHandles);
            stopWatch.Stop();
            
            Console.WriteLine($"ThreadPool - {stopWatch.Elapsed}");
        }

        private void LoadDataWithThreadPool(object? state)
        {
            var item = (ThreadPoolDataLoaderItem)state!;

            try
            {
                using var repository = new CustomerRepository();
                
                foreach (var customer in item.Customers)
                {
                    repository.AddCustomer(customer);
                }
                
                repository.SaveChanges();
            }
            finally
            {
                var autoResetEvent = (AutoResetEvent) item.WaitHandle;
                autoResetEvent.Set();
            }
        }


        private List<List<Customer>> CustomersSlice()
        {
            var chunkSize = _customers.Count / _threadCount;
            if (_customers.Count % _threadCount != 0)
            {
                ++chunkSize;
            }

            var customersChunks = new List<List<Customer>>();

            for (var i = 0; i < _customers.Count; i += chunkSize)
            {
                customersChunks.Add(GetCustomersChunk(_customers, i, chunkSize));
            }
            
            return customersChunks;
        }

        private static List<Customer> GetCustomersChunk(List<Customer> source, int start, int count)
        {
            if (start + count > source.Count)
            {
                count = source.Count - start;
            }
            var chunk = new Customer[count];
            source.CopyTo(start, chunk, 0, count);
            return chunk.ToList();
        }
    }
}