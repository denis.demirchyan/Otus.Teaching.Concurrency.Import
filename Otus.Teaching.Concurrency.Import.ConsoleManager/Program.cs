﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.ConsoleManager.Api;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.ConsoleManager
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var customerApi = new CustomerApi(new Uri("https://localhost:5001/"));
            var isRun = true;
            while (isRun)
            {
                ShowMenu();
                var action = Console.ReadLine();
                switch (action)
                {
                    case "1":
                    {
                        var customer = RandomCustomerGenerator.Generate();
                        Console.WriteLine("Customer:");
                        Console.WriteLine(customer.ToString());
                        var response = await customerApi.AddCustomer(customer);
                        Console.WriteLine(response.StatusCode);
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        break;
                    }
                    case "2":
                    {
                        var customer = GetCustomerFromConsole();
                        Console.WriteLine("Customer:");
                        Console.WriteLine(customer.ToString());
                        var response = await customerApi.AddCustomer(customer);
                        Console.WriteLine(response.StatusCode);
                        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                        break;
                    }
                    case "3":
                    {
                        Console.WriteLine("Введите идентификатор пользователя:");
                        var userId = int.Parse(Console.ReadLine()!);
                        var customer = await customerApi.GetCustomerById(userId);
                        Console.WriteLine(customer != null ? customer.ToString() : "Not found.");
                        break;
                    }
                    case "0":
                        isRun = false;
                        break;
                }
            }
        }

        static void ShowMenu()
        {
            Console.WriteLine("Выберите действие:");
            Console.WriteLine("1. Добавить случайного нового пользователя;");
            Console.WriteLine("2. Добавить нового пользователя;");
            Console.WriteLine("3. Получить пользователя по идентификатору;");
            Console.WriteLine("0. Завершение работы;");
        }

        static Customer GetCustomerFromConsole()
        {
            var customer = new Customer();
            
            Console.WriteLine("Введите идентификатор пользователя:");
            customer.Id = int.Parse(Console.ReadLine()!);
            Console.WriteLine("Введите ФИО пользователя:");
            customer.FullName = Console.ReadLine();
            Console.WriteLine("Введите почту пользователя:");
            customer.Email = Console.ReadLine();
            Console.WriteLine("Введите телефон пользователя:");
            customer.Phone = Console.ReadLine();
            
            return customer;
        }
    }
}