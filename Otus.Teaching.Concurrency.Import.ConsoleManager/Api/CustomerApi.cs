using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.ConsoleManager.Api
{
    public class CustomerApi
    {
        private readonly HttpClient _httpClient;

        public CustomerApi(Uri baseUrl)
        {
            var clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = 
                (sender, cert, chain, sslPolicyErrors) => { return true; };
            _httpClient = new HttpClient(clientHandler) { BaseAddress = baseUrl };
        }

        public async Task<HttpResponseMessage> AddCustomer(Customer customer)
        {
            return await _httpClient.PostAsJsonAsync("/users", customer);
        }

        public async Task<Customer> GetCustomerById(int id)
        {
            var response = await _httpClient.GetAsync($"/users/{id}");
            
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Customer>(content);
            }
            
            return null;
        }
    }
}