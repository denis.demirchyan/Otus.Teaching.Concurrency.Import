using System;

namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    public class Customer
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}{Environment.NewLine}" +
                   $"Fullname: {FullName}{Environment.NewLine}" +
                   $"Email: {Email}{Environment.NewLine}" +
                   $"Phone: {Phone}{Environment.NewLine}";
        }
    }
}