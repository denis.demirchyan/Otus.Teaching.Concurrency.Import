using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public class ParserFactory
    {
        public static IDataParser<List<Customer>> GetParser(string dataFilePath, string dataFileExtension)
        {
            return dataFileExtension switch
            {
                "csv" => new CsvParser($"{dataFilePath}.{dataFileExtension}"),
                "xml" => new XmlParser($"{dataFilePath}.{dataFileExtension}"),
                _ => throw new ArgumentException("File extension is not supported.")
            };
        }
    }
}