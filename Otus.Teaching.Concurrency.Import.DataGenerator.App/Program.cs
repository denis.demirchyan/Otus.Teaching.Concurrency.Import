﻿using System;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args, out var dataFileName, out var dataFileExtension,out var dataCount))
                return;
            
            Console.WriteLine("Создание данных...");

            var generator = GeneratorFactory.GetGenerator(dataFileName, dataFileExtension, dataCount);
            
            generator.Generate();
            
            Console.WriteLine($"Данные созданы в файле {dataFileName}.{dataFileExtension}.");
        }

        private static bool TryValidateAndParseArgs(string[] args, out string dataFileName, out string dataFileExtension, out int dataCount)
        {
            dataFileName = null;
            dataFileExtension = null;
            dataCount = 1000;

            if (args is { Length: > 0 })
            {
                dataFileName = args[0];
            }
            else
            {
                Console.WriteLine("Требуется передать полный путь к файлу без расширения.");
                return false;
            }

            if (args is { Length: > 1})
            {
                dataFileExtension = args[1];
            }
            else
            {
                Console.WriteLine("Требуется передать тип файла.");
                return false;
            }
            
            if (args.Length > 2 && !int.TryParse(args[2], out dataCount))
            {
                Console.WriteLine("Требуется передать количество записей числом.");
                return false;
            }

            return true;
        }
    }
}