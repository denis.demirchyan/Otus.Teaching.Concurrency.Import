using System;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(string fileName, string dataFileExtension, int dataCount)
        {
            return dataFileExtension switch
            {
                "csv" => new CsvGenerator($"{fileName}.{dataFileExtension}", dataCount),
                "xml" => new XmlDataGenerator($"{fileName}.{dataFileExtension}", dataCount),
                _ => throw new ArgumentException("File extension is not supported.")
            };
        }
    }
}