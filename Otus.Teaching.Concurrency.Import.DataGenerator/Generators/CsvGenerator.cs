using System.Globalization;
using System.IO;
using CsvHelper;
using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CsvGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = new StreamWriter(_fileName);
            using var csv = new CsvWriter(stream, CultureInfo.InvariantCulture);
            csv.WriteRecords(customers);
        }
    }
}