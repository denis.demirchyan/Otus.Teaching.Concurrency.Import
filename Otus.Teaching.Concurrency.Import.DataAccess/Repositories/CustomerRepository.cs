using System;
using System.Linq;
using Otus.Teaching.Concurrency.Import.DataAccess.RepositoriesContext;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository, IDisposable
    {
        private readonly PostgreCustomerContext _dbContext;

        public CustomerRepository()
        {
            _dbContext = new PostgreCustomerContext();
        }
        public void AddCustomer(Customer customer)
        {
            _dbContext.Customers.Add(customer);
        }

        public Customer GetCustomer(int id)
        {
            return _dbContext.Customers.FirstOrDefault(c => c.Id == id);
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
        }
    }
}