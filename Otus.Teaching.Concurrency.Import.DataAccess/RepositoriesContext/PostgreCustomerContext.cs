using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.RepositoriesContext
{
    public sealed class PostgreCustomerContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public PostgreCustomerContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseNpgsql("Host=localhost;Port=5432;Database=Customers;Username=pguser;Password=pgpassword")
                .UseSnakeCaseNamingConvention();
        }
    }
}