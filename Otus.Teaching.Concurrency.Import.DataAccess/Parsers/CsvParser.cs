using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        private readonly string _filePath;

        public CsvParser(string filePath)
        {
            _filePath = filePath;
        }

        public List<Customer> Parse()
        {
            using var stream = new StreamReader(_filePath);
            using var csv = new CsvReader(stream, CultureInfo.InvariantCulture);
            return csv.GetRecords<Customer>().ToList();
        }
    }
}