﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        private readonly string _filePath;
        
        public XmlParser(string filePath)
        {
            _filePath = filePath;
        }
        
        public List<Customer> Parse()
        {
            using var stream = File.OpenRead(_filePath);
            var xmlCustomers = new XmlSerializer(typeof(CustomersList)).Deserialize(stream) as CustomersList;
            return xmlCustomers?.Customers;
        }
    }
}